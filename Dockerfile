FROM openjdk:11.0-jre-slim

WORKDIR /opt/backend

COPY target/osworks-api-0.0.1-SNAPSHOT.jar /opt/backend/

EXPOSE 8080

CMD ["java", "-jar", "-Xmx256m" , "-Xms256m", "osworks-api-0.0.1-SNAPSHOT.jar"]
