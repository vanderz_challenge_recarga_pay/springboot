# springboot

## Introduction

This repository contains the application code, a Java Rest API. It was built to meet these requirements:

* Spring boot
* Containers
* Database relational and non-relational (rds/dynamo).

Two resources were built:

1) GET /clientes - it retrives some customer data: name and e-mail
2) GET /health - it retrieves application health check: "OK" if the application is running.

Maven is the tool used to build and to test the application package. It generates a JAR file.

This JAR file is copied to the docker container. You can find the Dockerfile in this repository.

It also was implemented database migration with Flyway tool. It is responsible to create relational database's table and populate it. This tool runs at application start up. The sql scripts are located at src/main/resources/db/migration.

The application.properties has entries for DynamoDB properties, but it was not coded any funcionality that uses that NoSQL database.

The API application is listened at port 8080.

# CI/CD pipeline

The root Group of this project contains some important variables. Only the group owner has access to them:

* AWS_ACCESS_KEY_ID: it contains the AWS access key of the user that will interact to AWS.
* AWS_SECRET_ACCESS_KEY: it contains the AWS secret access key of the user that will interact to AWS.
* AWS_DEFAULT_REGION: it contains the AWS default region where the AWS resources will be deployed.
* DB_NAME: Database name
* DB_PASS: Database user password
* DB_USER: Database user name

All the sensitive variables are masked, so it will not be possible to see their values at any job log.

The pipeline of this project works that way:

1) After pushing code to the master branch, a new pipeline with the job **job-test-package** will be triggered. This job builds and tests the package. At the end of this execution, it does an API call to check if the API is running right.

![Alt text](images/springboot-job-teste.png?raw=true "Job Test")

https://gitlab.com/vanderz_challenge_recarga_pay/springboot/-/jobs/990136608

2) After **job-test-package** job succeeds, a tag needs to be created. The tag has to have the following format: v[0-9].[0-9].[0-9]. This is the format that ECR will manage the container life cycle.

3) A new pipeline is triggered as soon as the tag was created. This new pipeline has one bridged job called **job-deploy-infra**, that will trigger a new pipeline at terraform/infra repository.

![Alt text](images/springboot-bridge-job.png?raw=true "Bridge Job")

https://gitlab.com/vanderz_challenge_recarga_pay/springboot/-/pipelines/247614325

4) The recently pipeline created at terraform/infra repository has five jobs:

* **init**: performs terraform init
* **validate**: performs terraform validate
* **build**: performs terraform plan
* **deploy**: performs terraform apply (manual execution)
* **cleanup**: performs terraform destroy (manual execution)

![Alt text](images/terraform-infra.png?raw=true "Pipeline to deploy VPC, RDS, DynamoDB, ECR, Cloudwatch and SNS")

https://gitlab.com/vanderz_challenge_recarga_pay/terraform/infra/-/pipelines/247614329

5) The **build** job will show the plan that terraform will apply at AWS. If everything is right, the **deploy** job needs to be executed manually.

6) The **deploy** job will perform the following deployment at AWS:

* VPC: deploys the network designed architecture
* RDS: deploys Mysql relational database
* DynamoDB: deploys DynamoDB nosql database
* ECR: deploys Container Registry and the repository where the container images will rely
* Cloudwatch: monitors and alarms RDS events
* SNS: sends e-mail notification when an alarm occur

7) Finishing successfully, the **deploy** job will trigger a new pipeline at this repository (springboot) passing the following variables:

* APP_VERSION: the API application image version, for docker commands when building and pushing the image and for terraform when creating ECS
* ECR_REPOSITORY_URL: the repository endpoint where the API image container will rely,  for docker commands when building and pushing the image and for terraform when creating ECS
* DB_HOST: the mysql host endpoint, for application when building the package
* DYNAMODB_ENDPOINT: the dynamodb endpoint, for application when building the package
* DYNAMO_DB: the dynamodb table, for application when building the package
* SUBNET_PRIVATE_1C: the subnet private id from one AZ, for terraform when creating ECS and NLB
* SUBNET_PRIVATE_1D: the subnet private id from other AZ, for terraform when creating ECS and NLB
* VPC: the vpc id, for terraform when creating ECS and NLB

8) This new pipeline has three jobs:

* **job-build-package**: build the API application package (JAR File)
* **job-docker-package**: build the API application image and push to ECR
* **job-deploy-springboot**: trigger a new pipeline at terraform/springboot repository

![Alt text](images/springboot-build-app.png?raw=true "Pipeline to build application")

https://gitlab.com/vanderz_challenge_recarga_pay/springboot/-/pipelines/247620948

9) The recently pipeline created at terraform/springboot repository also has five jobs:

* **init**: performs terraform init
* **validate**: performs terraform validate
* **build**: performs terraform plan
* **deploy**: performs terraform apply (manual execution)
* **cleanup**: performs terraform destroy (manual execution)

![Alt text](images/terraform-springboot.png?raw=true "Pipeline to deploy ECS, NLB, API Gateway and WAF")

https://gitlab.com/vanderz_challenge_recarga_pay/terraform/springboot/-/pipelines/247625124

10) The **build** job will show the plan that terraform will apply at AWS. If everything is right, the **deploy** job needs to be executed manually.

11) The **deploy** job will perform the following deployment at AWS:

* ECS: deploys the API application into an ECS cluster
* NLB: load balances the ECS ingress traffic
* API Gateway: deploys API controls to the API application resources
* WAF: protects the API calls against some harmful web actions, like SQL Injection

12) At the end of this execution, it does an API call to check if the API is running right at AWS.

![Alt text](images/api-running-check.png?raw=true "API running check")

https://gitlab.com/vanderz_challenge_recarga_pay/terraform/springboot/-/jobs/990186968

13) You can destroy all the resources created in AWS by these CI/CD pipelines just triggering manually the **cleanup** job in each terraform repository pipeline 

**terraform/infra**

![Alt text](images/terraform-destroy-infra.png?raw=true "Destroying Infra AWS Resources")

https://gitlab.com/vanderz_challenge_recarga_pay/terraform/infra/-/jobs/990145988

**terraform/springboot**

![Alt text](images/terraform-destroy-springboot.png?raw=true "Destroying Springboot AWS Resources")

https://gitlab.com/vanderz_challenge_recarga_pay/terraform/springboot/-/jobs/990187000